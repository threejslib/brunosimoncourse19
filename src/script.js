import "./style.css"
import * as THREE from "three"
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js"
import * as dat from "dat.gui"

/**
 * Base
 */
// Debug
const gui = new dat.GUI()

// Canvas
const canvas = document.querySelector("canvas.webgl")

// Scene
const scene = new THREE.Scene()

/**
 * Objects
 */
const object1 = new THREE.Mesh(
  new THREE.SphereGeometry(0.5, 16, 16),
  new THREE.MeshBasicMaterial({ color: "#ff0000" })
)
object1.position.x = -2

const object2 = new THREE.Mesh(
  new THREE.SphereGeometry(0.5, 16, 16),
  new THREE.MeshBasicMaterial({ color: "#ff0000" })
)

const object3 = new THREE.Mesh(
  new THREE.SphereGeometry(0.5, 16, 16),
  new THREE.MeshBasicMaterial({ color: "#ff0000" })
)
object3.position.x = 2

scene.add(object1, object2, object3)

/**
 * Raycaster
 */
const raycaster = new THREE.Raycaster()

// const rayOirign = new THREE.Vector3(-3, 0, 0)
// const rayDirection = new THREE.Vector3(10, 0, 0)
// rayDirection.normalize()
// raycaster.set(rayOirign, rayDirection)

// const intersect = raycaster.intersectObject(object2)

// const intersects = raycaster.intersectObjects([object1, object2, object3])

// console.log(intersect)
// console.log(intersects)

/**
 * Sizes
 */
const sizes = {
  width: window.innerWidth,
  height: window.innerHeight,
}

window.addEventListener("resize", () => {
  // Update sizes
  sizes.width = window.innerWidth
  sizes.height = window.innerHeight

  // Update camera
  camera.aspect = sizes.width / sizes.height
  camera.updateProjectionMatrix()

  // Update renderer
  renderer.setSize(sizes.width, sizes.height)
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(
  75,
  sizes.width / sizes.height,
  0.1,
  100
)
camera.position.z = 3
scene.add(camera)

/**
 * Mouse
 */
const mouse = new THREE.Vector2()

window.addEventListener("mousemove", (event) => {
  //   console.log("mouse move")

  mouse.x = (event.clientX / sizes.width) * 2 - 1
  //   console.log(mouse.x)
  mouse.y = -(event.clientY / sizes.height) * 2 + 1
  //   console.log(mouse.y)
})

window.addEventListener("click", () => {
  if (currentIntersect) {
    if (currentIntersect.object === object1) {
      console.log("ini bola 1")
    }
    if (currentIntersect.object === object2) {
      console.log("ini bola 2")
    }
    if (currentIntersect.object === object3) {
      console.log("ini bola 3")
    }
  }
})

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
  canvas: canvas,
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

/**
 * Animate
 */
const clock = new THREE.Clock()

/**
 * witness
 */
let currentIntersect = null

const tick = () => {
  const elapsedTime = clock.getElapsedTime()

  //Animate object
  object1.position.y = Math.sin(elapsedTime * 0.3) * 1.5
  object2.position.y = Math.sin(elapsedTime * 0.3 + 2) * 1.5
  object3.position.y = Math.sin(elapsedTime * 0.3 + 3) * 1.5

  //Raycaster

  raycaster.setFromCamera(mouse, camera)

  //   const rayOrigin = new THREE.Vector3(-3, 0, 0)
  //   const rayDirection = new THREE.Vector3(1, 0, 0)
  //   rayDirection.normalize()

  const objectsToTests = [object1, object2, object3]
  const intersects = raycaster.intersectObjects(objectsToTests)
  //     console.log(intersects)

  for (const object of objectsToTests) {
    object.material.color.set("#ff0000")
  }

  for (const intersect of intersects) {
    intersect.object.material.color.set("#0000ff")
  }

  if (intersects.length) {
    if (currentIntersect === null) {
      console.log("mouseenter")
    }
    currentIntersect = intersects[0]
  } else {
    if (currentIntersect) {
      console.log("mouse leave")
    }
    currentIntersect = null
  }

  //   raycaster.set(rayOrigin, rayDirection)

  // Update controls
  controls.update()

  // Render
  renderer.render(scene, camera)

  // Call tick again on the next frame
  window.requestAnimationFrame(tick)
}

tick()
